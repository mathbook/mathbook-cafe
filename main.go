package main

import (
	"embed"
	"fmt"
	"io/fs"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"text/template"
	"time"
)

//go:embed resources/*
var resources embed.FS

var templates = template.Must(template.ParseFS(resources, "resources/*.html"))

var files fs.FS

func init() {
	var err error
	files, err = fs.Sub(resources, "resources/static")
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

	err = fs.WalkDir(files, ".", func(path string, d fs.DirEntry, err error) error {
		if d.Type().IsRegular() {
			slog.Info(filepath.Join(path))
		}
		return nil
	})
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
}

func main() {
	mux := http.NewServeMux()

	mux.Handle("/files/", http.StripPrefix("/files", http.FileServer(http.FS(files))))

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/welcome", http.StatusMovedPermanently)
	})

	mux.HandleFunc("/welcome", func(w http.ResponseWriter, r *http.Request) {
		if err := templates.ExecuteTemplate(w, "welcome.html", Default); err != nil {
			slog.Error(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	})

	slog.Debug("listening")
	if err := http.ListenAndServe(":8080", mux); err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
}

var Default = Page{
	Head: Head{
		Title:          "Mathbook Cafe",
		Author:         "Jared Davis",
		Katex:          true,
		HideFromRobots: false,
	},
	Footer: Footer{
		Copyright: fmt.Sprintf("&copy; Jared Davis %s - All Rights Reserved", time.Now().Format("2006")),
	},
}

type Page struct {
	Head
	Footer
}

type Head struct {
	Title          string
	Description    string
	Author         string
	Katex          bool
	HideFromRobots bool
}

type Footer struct {
	Copyright string
}
